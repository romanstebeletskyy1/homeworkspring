package org.example.service;

import org.example.domain.Account;
import org.example.domain.Company;
import org.example.repository.CompanyRepository;
import org.example.service.CompanyService;
import org.example.service.DefaultCompanyService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CompanyServiceTest {
    @Captor
    private ArgumentCaptor<Company> companyArgumentCaptor;
    @Mock
    private CompanyRepository companyRepository;

    @InjectMocks
    private DefaultCompanyService companyService;

    @Test
    public void testFindAll() {
        Company company = new Company();
        Company company1 = new Company();
        List<Company> companies = List.of(company1, company);
        when(companyRepository.findAll())
                .thenReturn(companies);

        List<Company> companyList = companyService.findAll();
        assertNotNull(companyList);
        assertFalse(companyList.isEmpty());
        assertIterableEquals(companies, companyList);
    }

    @Test
    public void testSave() {
        Company company = new Company();

        companyService.save(company);

        verify(companyRepository).save(companyArgumentCaptor.capture());
        Company company1 = companyArgumentCaptor.getValue();
        assertEquals(company, company1);
    }
}
