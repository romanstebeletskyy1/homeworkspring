package org.example.service;

import org.example.domain.Account;
import org.example.domain.Currency;
import org.example.domain.Customer;
import org.example.repository.AccountRepository;
import org.example.service.DefaultAccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
    @Captor
    private ArgumentCaptor<Account> accountArgumentCaptor;

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private DefaultAccountService accountService;

    @Test
    public void testFindAll() {
        Account account = new Account();
        Account account1 = new Account();
        List<Account> accounts = List.of(account, account1);
        when(accountRepository.findAll())
                .thenReturn(accounts);

        List<Account> accountsActual = accountService.findAll();
        assertNotNull(accountsActual);
        assertFalse(accountsActual.isEmpty());
        assertIterableEquals(accounts, accountsActual);
    }

    @Test
    public void testWithdrawMoney(){
        Customer customer = new Customer();
        Account account = new Account( Currency.UAH, customer);
        account.setBalance(50.50);
        account.setNumber("1111111");
        account.setId(1L);
        when(accountRepository.findByNumber("1111111"))
                .thenReturn(account);
        Boolean AccountDepositResult = accountService.withdrawMoney("1111111", 10.50);

        verify(accountRepository).save(accountArgumentCaptor.capture());
        Account accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account, accountActualArgument);
        assertEquals(true, AccountDepositResult);
        assertEquals(40.00, account.getBalance());
    }

    @Test
    public void testRefillMoney(){
        Customer customerExpected = new Customer();
        Account account = new Account(Currency.UAH,customerExpected);
        account.setBalance(0D);
        account.setNumber("1111111");
        account.setId(1L);
        when(accountRepository.findByNumber("1111111"))
                .thenReturn(account);
        Boolean AccountDepositResult = accountService.refillAccount("1111111", 50.20);

        verify(accountRepository).save(accountArgumentCaptor.capture());
        Account accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account, accountActualArgument);
        assertEquals(true, AccountDepositResult);
        assertEquals(50.20, account.getBalance());
    }

    @Test
    public void testTransferMoney(){
        Customer customerExpected = new Customer();
        Account accountSender = new Account(Currency.UAH, customerExpected);
        accountSender.setBalance(50.50);
        accountSender.setNumber("1111111");
        accountSender.setId(1L);
        Account accountReceiver = new Account(Currency.UAH, customerExpected);
        accountReceiver.setBalance(0D);
        accountReceiver.setNumber("2222222");
        accountReceiver.setId(2L);
        when(accountRepository.findByNumber("1111111"))
                .thenReturn(accountSender);
        when(accountRepository.findByNumber("2222222"))
                .thenReturn(accountReceiver);
        Boolean AccountDepositResult = accountService.transferMoney("1111111", "2222222",50.50);

        verify(accountRepository, times(2)).save(accountArgumentCaptor.capture());
        List<Account> accountActualArgument = accountArgumentCaptor.getAllValues();
        assertTrue(accountActualArgument.containsAll(List.of(accountSender, accountReceiver)));
        assertEquals(true, AccountDepositResult);
        assertEquals(0D, accountSender.getBalance());
        assertEquals(50.50, accountReceiver.getBalance());
    }
}
