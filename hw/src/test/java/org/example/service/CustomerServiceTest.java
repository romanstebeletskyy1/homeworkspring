package org.example.service;

import org.example.domain.Account;
import org.example.domain.Company;
import org.example.domain.Customer;
import org.example.repository.AccountRepository;
import org.example.repository.CustomerRepository;
import org.example.service.DefaultCustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {
    @Captor
    private ArgumentCaptor<Customer> customerArgumentCaptor;
@Mock
    private CustomerRepository customerRepository;
@Mock
    private AccountRepository accountRepository;

@InjectMocks
private DefaultCustomerService customerService;
    @Test
    public void testFindAll() {
        Customer customer = new Customer();
        Customer customer1 = new Customer();
        List<Customer> customers = List.of(customer, customer1);
        when(customerRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(customers));

        List<Customer> customerList = customerService.findAll(1,2);
        assertNotNull(customerList);
        assertFalse(customerList.isEmpty());
        assertIterableEquals(customers, customerList);
    }

    @Test
    public void testSave() {
        Customer customer = new Customer();

        customerService.save(customer);

        verify(customerRepository).save(customerArgumentCaptor.capture());
        Customer customer1 = customerArgumentCaptor.getValue();
        assertEquals(customer, customer1);
    }
}
