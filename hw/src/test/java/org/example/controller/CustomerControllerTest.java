package org.example.controller;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.NonNull;
import org.example.controller.CustomerRestController;
import org.example.domain.Customer;
import org.example.domain.SysRole;
import org.example.domain.SysUser;
import org.example.domain.dto.CustomerDtoRequest;
import org.example.domain.dto.CustomerDtoResponse;
import org.example.filter.JwtFilter;
import org.example.service.*;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("local")
@WithMockUser(username = "a", roles = {"USER"})
@WebMvcTest(CustomerRestController.class)
public class CustomerControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CustomerService customerService;
    @MockBean
    private CustomerDtoResponseMapper customerDtoResponseMapper;
    @MockBean
    private CustomerDtoRequestMapper customerDtoRequestMapper;
//    @Autowired
//    private JwtFilter jwtFilter;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @MockBean
    private UserService userService;
    @MockBean
    JwtProvider jwtProvider;


@TestConfiguration
    static class TestConfig{
//    @Bean
//    public JwtFilter jwtFilter(){
//        return new JwtFilter(new JwtProvider("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V",
//                "LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V"));
//    }

}

    public String generateAccessToken(@NonNull SysUser user) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant accessExpirationInstant = now.plusMinutes(5).atZone(ZoneId.systemDefault()).toInstant();
        final Date accessExpiration = Date.from(accessExpirationInstant);
        return Jwts.builder()
                .setSubject(user.getUserName())
                .setExpiration(accessExpiration)
                .signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2Vq+PjYENhgTJGXLNm7YS4f1+IMw==")))
                .claim("roles", user.getRoles())
                .claim("firstName", user.getUserName())
                .compact();
    }


    @Test
    public void testFindAll() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        Customer customer = new Customer();
        CustomerDtoResponse customerDtoResponse = new CustomerDtoResponse();
        customerDtoResponse.setName("GGG");
        customerDtoResponse.setId(1L);
        when(customerService.findAll(1, 10))
                .thenReturn(List.of(customer));
        when(customerDtoResponseMapper.convertToDto(customer)).thenReturn(customerDtoResponse);



         mockMvc.perform(MockMvcRequestBuilders.get("/customers/")
                        .contentType(MediaType.APPLICATION_JSON)
                         .header("Authorization", "Bearer "+this.generateAccessToken(sysUser)))
                .andExpect(status().isOk())
                 .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("GGG")));
    }


    @Test
    public void testCreateCustomer() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));
        Customer customer = new Customer();
        CustomerDtoResponse customerDtoResponse = new CustomerDtoResponse();
        customerDtoResponse.setName("Adam");
        customerDtoResponse.setEmail("adam@gmail.com");
        customerDtoResponse.setAge(40);
        customerDtoResponse.setPhone("+380671112233");
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        when(customerService.save(any())).thenReturn(customer);

        when(customerDtoResponseMapper.convertToDto(customer)).thenReturn(customerDtoResponse);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/customers/")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+this.generateAccessToken(sysUser))
                        .content(
                                """
                                        {
                                            "name": "Adam",
                                            "email": "adam@gmail.com",
                                            "age": 40,
                                            "phone": "+380671112233",
                                            "password": "22233322211dd"
                                        }
                                        """
                        ))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Adam")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age", Matchers.is(40)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.is("adam@gmail.com")));
    }

    @Test
    public void testDeleteCustomer() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));
        Customer customer = new Customer();

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));
when(customerService.getOne(1)).thenReturn(customer);
        when(customerService.delete(customer))
                .thenReturn(true);

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/customers/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser)))
                .andExpect(status().isOk());
    }
}


