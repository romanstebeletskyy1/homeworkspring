package org.example.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.NonNull;
import org.example.domain.Company;
import org.example.domain.Customer;
import org.example.domain.SysRole;
import org.example.domain.SysUser;
import org.example.domain.dto.CompanyDtoResponse;
import org.example.domain.dto.CustomerDtoResponse;
import org.example.service.*;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("local")
@WithMockUser(username = "a", roles = {"USER"})
@WebMvcTest(CompanyRestController.class)
public class CompanyControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CompanyService companyService;
    @MockBean
    private  CompanyDtoResponseMapper companyDtoResponseMapper;
    @MockBean
    private  CompanyDtoRequestMapper companyDtoRequestMapper;
    //    @Autowired
//    private JwtFilter jwtFilter;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @MockBean
    private UserService userService;
    @MockBean
    JwtProvider jwtProvider;


    @TestConfiguration
    static class TestConfig{
//    @Bean
//    public JwtFilter jwtFilter(){
//        return new JwtFilter(new JwtProvider("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V",
//                "LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V"));
//    }

    }

    public String generateAccessToken(@NonNull SysUser user) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant accessExpirationInstant = now.plusMinutes(5).atZone(ZoneId.systemDefault()).toInstant();
        final Date accessExpiration = Date.from(accessExpirationInstant);
        return Jwts.builder()
                .setSubject(user.getUserName())
                .setExpiration(accessExpiration)
                .signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2Vq+PjYENhgTJGXLNm7YS4f1+IMw==")))
                .claim("roles", user.getRoles())
                .claim("firstName", user.getUserName())
                .compact();
    }


    @Test
    public void testFindAll() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        Company company = new Company();
        CompanyDtoResponse companyDtoResponse = new CompanyDtoResponse();
        companyDtoResponse.setName("GGG");
        companyDtoResponse.setId(1L);
        when(companyService.findAll())
                .thenReturn(List.of(company));
        when(companyDtoResponseMapper.convertToDto(company)).thenReturn(companyDtoResponse);



        mockMvc.perform(MockMvcRequestBuilders.get("/companies/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+this.generateAccessToken(sysUser)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("GGG")));
    }


    @Test
    public void testCreateCompany() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));
        Company company = new Company();
        CompanyDtoResponse companyDtoResponse = new CompanyDtoResponse();
        companyDtoResponse.setName("TESLA");
        companyDtoResponse.setAddress("ZYTOMYR");
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        when(companyService.save(any())).thenReturn(company);

        when(companyDtoResponseMapper.convertToDto(company)).thenReturn(companyDtoResponse);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/companies/")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+this.generateAccessToken(sysUser))
                        .content(
                                """
                                        {
                                            "name": "TESLA",
                                            "address": "ZYTOMYR"
                                        }
                                        """
                        ))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("TESLA")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address", Matchers.is("ZYTOMYR")));
    }

    @Test
    public void testDeleteCustomer() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));
        Company company = new Company();

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));
        when(companyService.getOne(1)).thenReturn(company);

        when(companyService.delete(1L)).thenReturn(true);

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/companies/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + this.generateAccessToken(sysUser)))
                .andExpect(status().isOk());
    }
}
