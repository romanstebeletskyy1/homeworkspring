package org.example.controller;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.NonNull;
import org.example.domain.Account;
import org.example.domain.Company;
import org.example.domain.SysRole;
import org.example.domain.SysUser;
import org.example.domain.dto.AccountDtoResponse;
import org.example.domain.dto.CompanyDtoResponse;
import org.example.service.*;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("local")
@WithMockUser(username = "a", roles = {"USER"})
@WebMvcTest(AccountRestController.class)
public class AccountControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AccountDtoResponseMapper accountDtoResponseMapper;
    @MockBean
    private AccountDtoRequestMapper accountDtoRequestMapper;
    @MockBean
    private AccountService accountService;
    //    @Autowired
//    private JwtFilter jwtFilter;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @MockBean
    private UserService userService;
    @MockBean
    JwtProvider jwtProvider;


    @TestConfiguration
    static class TestConfig{
//    @Bean
//    public JwtFilter jwtFilter(){
//        return new JwtFilter(new JwtProvider("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V",
//                "LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2V"));
//    }

    }

    public String generateAccessToken(@NonNull SysUser user) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant accessExpirationInstant = now.plusMinutes(5).atZone(ZoneId.systemDefault()).toInstant();
        final Date accessExpiration = Date.from(accessExpirationInstant);
        return Jwts.builder()
                .setSubject(user.getUserName())
                .setExpiration(accessExpiration)
                .signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode("LejjnLZua6SlR7eZXByD2+9M5P+dYxK3IlfA6XgPksuXijiXMAcpulI03o2Vq+PjYENhgTJGXLNm7YS4f1+IMw==")))
                .claim("roles", user.getRoles())
                .claim("firstName", user.getUserName())
                .compact();
    }
    @Test
    public void testFindAll() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        Account account = new Account();
        AccountDtoResponse accountDtoResponse = new AccountDtoResponse();
        accountDtoResponse.setNumber("GGG");
        accountDtoResponse.setId(1L);
        when(accountService.findAll())
                .thenReturn(List.of(account));
        when(accountDtoResponseMapper.convertToDto(account)).thenReturn(accountDtoResponse);



        mockMvc.perform(MockMvcRequestBuilders.get("/accounts/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+this.generateAccessToken(sysUser)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].number", Matchers.is("GGG")));
    }
    @Test
    public void testWithdrawMoney() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));
        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        when(accountService.withdrawMoney("wwwaaa",100)).thenReturn(true);
        mockMvc.perform(MockMvcRequestBuilders.put("/accounts/withdraw")
                        .with(csrf())
                        .param("number","wwwaaa")
                        .param("sum","100")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+this.generateAccessToken(sysUser)))
                .andExpect(status().isOk());
    }
    @Test
    public void testRefillMoney() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        when(accountService.refillAccount("wwwaaa",100)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.put("/accounts/refill")
                        .with(csrf())
                        .param("number","wwwaaa")
                        .param("sum","100")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+this.generateAccessToken(sysUser)))
                .andExpect(status().isOk());
    }
    @Test
    public void testTransferMoney() throws Exception {
        SysUser sysUser = new SysUser(101L, "a", passwordEncoder.encode("a"), true, new HashSet<>(List.of(new SysRole(101L, "USER", new SysUser()))));

        when(userService.getByLogin(any(String.class)))
                .thenReturn(Optional.of(sysUser));

        when(accountService.transferMoney("wwwaaa","qwqwqqw",100)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.put("/accounts/transfer")
                        .with(csrf())
                        .param("numberFrom","wwwaaa")
                        .param("numberTo","wwwaaa")
                        .param("sum","100")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+this.generateAccessToken(sysUser)))
                .andExpect(status().isOk());
    }
}
