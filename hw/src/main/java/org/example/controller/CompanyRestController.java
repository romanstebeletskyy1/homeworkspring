package org.example.controller;

import jakarta.validation.constraints.Min;
import org.example.domain.Company;
import org.example.domain.dto.CompanyDtoRequest;
import org.example.domain.dto.CompanyDtoResponse;
import org.example.service.CompanyDtoRequestMapper;
import org.example.service.CompanyDtoResponseMapper;
import org.example.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@RestController
@RequestMapping("/companies")
public class CompanyRestController {
    private final CompanyDtoResponseMapper companyDtoResponseMapper;
    private final CompanyDtoRequestMapper companyDtoRequestMapper;
    private final CompanyService companyService;

    public CompanyRestController(CompanyDtoResponseMapper companyDtoResponseMapper, CompanyDtoRequestMapper companyDtoRequestMapper, CompanyService companyService) {
        this.companyDtoResponseMapper = companyDtoResponseMapper;
        this.companyDtoRequestMapper = companyDtoRequestMapper;
        this.companyService = companyService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        List<Company> companies = companyService.findAll();
        List<CompanyDtoResponse> dtos = companies.stream().map(companyDtoResponseMapper::convertToDto).toList();
        return ResponseEntity.ok(dtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id")@Min(0) Long id) {
        Company company = companyService.getOne(id);
        CompanyDtoResponse dto = companyDtoResponseMapper.convertToDto(company);
        if (company != null) {
            return ResponseEntity.ok(dto);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<CompanyDtoResponse> create(@RequestBody @Validated CompanyDtoRequest companyDto) {
        Company company =companyService.save(companyDtoRequestMapper.convertToEntity(companyDto));
       return ResponseEntity.ok().body(companyDtoResponseMapper.convertToDto(company));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id")@Min(0) Long id) {
        boolean result = companyService.delete(id);
        return result? ResponseEntity.ok().body("Deleted successfully"):ResponseEntity.notFound().build();
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id")@Min(0) Long id, @RequestBody @Validated CompanyDtoRequest companyDto) {
        Company company = companyDtoRequestMapper.convertToEntity(companyDto);
        company.setId(id);
        boolean result = companyService.update(company);
        if (result) {
            return ResponseEntity.ok().body("Changed successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }
}
