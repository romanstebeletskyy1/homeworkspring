package org.example.controller;

import jakarta.validation.constraints.Min;
import org.example.domain.Account;
import org.example.domain.Company;
import org.example.domain.dto.AccountDtoResponse;
import org.example.domain.dto.CompanyDtoResponse;
import org.example.service.AccountDtoRequestMapper;
import org.example.service.AccountDtoResponseMapper;
import org.example.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@RestController
@RequestMapping("/accounts")
public class AccountRestController {
    private AccountDtoResponseMapper accountDtoResponseMapper;
    private AccountDtoRequestMapper accountDtoRequestMapper;
    private AccountService accountService;

    public AccountRestController(AccountDtoResponseMapper accountDtoResponseMapper, AccountDtoRequestMapper accountDtoRequestMapper, AccountService accountService) {
        this.accountDtoResponseMapper = accountDtoResponseMapper;
        this.accountDtoRequestMapper = accountDtoRequestMapper;
        this.accountService = accountService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        List<Account> companies = accountService.findAll();
        List<AccountDtoResponse> dtos = companies.stream().map(accountDtoResponseMapper::convertToDto).toList();
        return ResponseEntity.ok(dtos);
    }

    @PutMapping("/withdraw")
    public ResponseEntity<?> withdraw(String number,@Min(0) double sum) {
        boolean result = accountService.withdrawMoney(number, sum);
        if (result) {
            return ResponseEntity.ok().body("Money were withdrawn successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }

    @PutMapping("/refill")
    public ResponseEntity<?> refill(String number,@Min(0) double sum) {
        boolean result = accountService.refillAccount(number, sum);
        if (result) {
            return ResponseEntity.ok().body("+Money");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }

    @PutMapping("/transfer")
    public ResponseEntity<?> transfer(String numberFrom, String numberTo,@Min(0) double sum) {
        boolean result = accountService.transferMoney(numberFrom, numberTo, sum);
        if (result) {
            return ResponseEntity.ok().body("Money were transferred successfully");
        }
        return ResponseEntity.ok().body("Nothing changed");
    }
}
