package org.example.service;

import org.example.domain.Company;
import org.example.domain.dto.CompanyDtoResponse;
import org.springframework.stereotype.Service;

@Service
public class CompanyDtoResponseMapper extends DtoMapperFacade<Company, CompanyDtoResponse> {
    public CompanyDtoResponseMapper() {
        super(Company.class, CompanyDtoResponse.class);
    }

    @Override
    protected void decorateDto(CompanyDtoResponse dto, Company entity){
        dto.setAddress(entity.getAddress());
        dto.setName(entity.getName());
        dto.setId(entity.getId());
    }
}
