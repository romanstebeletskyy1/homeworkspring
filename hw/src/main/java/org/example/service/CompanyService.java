package org.example.service;

import org.example.domain.Company;

import java.util.List;

public interface CompanyService {
    List<Company> findAll();

    void saveAll(List<Company> companies);

    void deleteAll(List<Company> companies);


    boolean delete(Long id);

    Company save(Company company);

    Company getOne(long id);

    boolean update(Company company);
}
