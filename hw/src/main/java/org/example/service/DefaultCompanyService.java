package org.example.service;

import jakarta.transaction.Transactional;
import org.example.repository.CompanyRepository;
import org.example.repository.CustomerCompanyRepository;
import org.example.domain.Company;
import org.springframework.stereotype.Service;

import java.util.List;

@Transactional
@Service
public class DefaultCompanyService implements CompanyService{
    private CompanyRepository companyRepository;
    private CustomerCompanyRepository customerCompanyRepository;

    public DefaultCompanyService(CompanyRepository companyRepository, CustomerCompanyRepository customerCompanyRepository) {
        this.companyRepository = companyRepository;
        this.customerCompanyRepository = customerCompanyRepository;
    }

    @Override
    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    @Override
    public void saveAll(List<Company> companies) {
        companyRepository.saveAll(companies);
    }

    @Override
    public void deleteAll(List<Company> companies) {
        companyRepository.deleteAll(companies);
    }

    @Override
    public boolean delete(Long id) {
        customerCompanyRepository.deleteByCompanyId(id);
        companyRepository.delete(companyRepository.findById(id).get());
        return  true;
    }

    @Override
    public Company save(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public Company getOne(long id) {
        return companyRepository.findById(id).get();
    }

    @Override
    public boolean update(Company company) {
         companyRepository.save(company);
        return true;
    }
}
