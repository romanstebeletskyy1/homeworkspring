package org.example.service;

import org.example.domain.Account;

import java.util.List;

public interface AccountService {
    List<Account> findAll();

    void saveAll(List<Account> accounts);

    void deleteAll(List<Account> accounts);

    void delete(Account account);

    Account save(Account account);

    Account getOne(long id);
    Account getOne(String number);
    boolean update(Account account);

    boolean refillAccount(String number, double sum);

    boolean withdrawMoney(String number, double sum);

    boolean transferMoney(String from, String to, double sum);
}
