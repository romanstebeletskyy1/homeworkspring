package org.example.service;

import org.example.domain.Currency;
import org.example.domain.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> findAll(Integer page, Integer size);


    void saveAll(List<Customer> customers);

    void deleteAll(List<Customer> customers);

    boolean delete(Customer customer);

    Customer save(Customer customer);

    Customer getOne(long id);
    Customer update(Customer customer);

    boolean createAccount(long id, Currency currency);


    boolean deleteAccount(long id, String number);
}
