package org.example.service;


import io.jsonwebtoken.Claims;
import jakarta.security.auth.message.AuthException;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.example.domain.SysUser;
import org.example.domain.jwt.JwtAuthentication;
import org.example.domain.jwt.JwtRequest;
import org.example.domain.jwt.JwtResponse;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Getter
public class AuthService {
    private final UserService userService;
    private final Map<String, String> refreshStorage = new HashMap<>();
    private final JwtProvider jwtProvider;

    private final PasswordEncoder passwordEncoder;

    public JwtResponse login(@NonNull JwtRequest authRequest) throws jakarta.security.auth.message.AuthException {
        final SysUser user = userService.getByLogin(authRequest.getLogin())
                .orElseThrow(() -> new jakarta.security.auth.message.AuthException("User not found"));
        if (passwordEncoder.matches(authRequest.getPassword(), user.getEncryptedPassword())){
            final String accessToken = jwtProvider.generateAccessToken(user);
            final String refreshToken = jwtProvider.generateRefreshToken(user);
            refreshStorage.put(user.getUserName(), refreshToken);
            return new JwtResponse(accessToken, refreshToken);
        } else {
            throw new jakarta.security.auth.message.AuthException("Password is incorrect");
        }
    }

    public JwtResponse getAccessToken(@NonNull String refreshToken) throws jakarta.security.auth.message.AuthException {
        if (jwtProvider.validateRefreshToken(refreshToken)) {
            final Claims claims = jwtProvider.getRefreshClaims(refreshToken);
            final String login = claims.getSubject();
            final String saveRefreshToken = refreshStorage.get(login);
            if (saveRefreshToken != null && saveRefreshToken.equals(refreshToken)) {
                final SysUser user = userService.getByLogin(login)
                        .orElseThrow(() -> new jakarta.security.auth.message.AuthException("User not found"));
                final String accessToken = jwtProvider.generateAccessToken(user);
                return new JwtResponse(accessToken, null);
            }
        }
        return new JwtResponse(null, null);
    }

    public JwtResponse refresh(@NonNull String refreshToken) throws jakarta.security.auth.message.AuthException {
        if (jwtProvider.validateRefreshToken(refreshToken)) {
            final Claims claims = jwtProvider.getRefreshClaims(refreshToken);
            final String login = claims.getSubject();
            final String saveRefreshToken = refreshStorage.get(login);
            if (saveRefreshToken != null && saveRefreshToken.equals(refreshToken)) {
                final SysUser user = userService.getByLogin(login)
                        .orElseThrow(() -> new jakarta.security.auth.message.AuthException("User not found"));
                final String accessToken = jwtProvider.generateAccessToken(user);
                final String newRefreshToken = jwtProvider.generateRefreshToken(user);
                refreshStorage.put(user.getUserName(), newRefreshToken);
                return new JwtResponse(accessToken, newRefreshToken);
            }
        }
        throw new AuthException("JWT token is invalid");
    }

    public JwtAuthentication getAuthInfo() {
        return (JwtAuthentication) SecurityContextHolder.getContext().getAuthentication();
    }

}
