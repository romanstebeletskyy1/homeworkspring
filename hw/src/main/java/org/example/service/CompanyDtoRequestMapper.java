package org.example.service;

import org.example.domain.Company;
import org.example.domain.dto.CompanyDtoRequest;
import org.springframework.stereotype.Service;

@Service
public class CompanyDtoRequestMapper extends DtoMapperFacade<Company, CompanyDtoRequest> {
    public CompanyDtoRequestMapper() {
        super(Company.class, CompanyDtoRequest.class);
    }
    @Override
    protected void decorateEntity(Company entity, CompanyDtoRequest dto){
        entity.setAddress(dto.getAddress());
        entity.setName(dto.getName());
    }
}
