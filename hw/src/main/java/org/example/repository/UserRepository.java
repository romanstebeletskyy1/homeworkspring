package org.example.repository;


import org.example.domain.SysUser;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<SysUser, Long> {
    Optional<SysUser> findUsersByUserName(String userName);

}
