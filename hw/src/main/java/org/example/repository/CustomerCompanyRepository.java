package org.example.repository;

import org.example.domain.CustomerCompany;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface CustomerCompanyRepository extends JpaRepository<CustomerCompany,Long> {
@Query("DELETE FROM CustomerCompany cc WHERE cc.companyId = :id")
@Modifying
    void deleteByCompanyId(Long id);
}
