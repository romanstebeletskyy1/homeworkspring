package org.example.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.*;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "companies")
public class Company extends AbstractEntity{
    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;

    @JsonIgnore
    @ManyToMany(mappedBy = "companies")
    private List<Customer> customers;
}
