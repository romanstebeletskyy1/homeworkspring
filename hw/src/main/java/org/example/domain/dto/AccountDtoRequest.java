package org.example.domain.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.domain.Currency;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountDtoRequest {
    private Currency currency;

    @NotNull
    @Min(value = 0, message = "balance should be 0+")
    private Double balance;
}
