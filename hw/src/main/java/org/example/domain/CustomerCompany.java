package org.example.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "customers_companies")
public class CustomerCompany extends AbstractEntity {
    @Column(name = "customer_id")
    private Long customerId;
    @Column(name = "company_id")
    private Long companyId;
}
