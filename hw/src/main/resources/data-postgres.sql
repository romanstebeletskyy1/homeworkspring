BEGIN;
INSERT INTO public.customers (name,email,age,phone,password) VALUES ('Anton','anton@gmail.com',20,'777888','JBDbde');
INSERT INTO public.customers (name,email,age,phone,password) VALUES ('Andriy','andriy@gmail.com',30,'54656','rbdGdrbb');
INSERT INTO public.customers (name,email,age,phone,password) VALUES ('Igor','igor@gmail.com',40,'346346','sAdgrg');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('John Smith', 'john.smith@gmail.com', 28, '555-1234', 'Passw0rd1');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Alice Johnson', 'alice.j@gmail.com', 35, '555-5678', 'Secure123');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('David Williams', 'david.w@gmail.com', 22, '555-9876', 'P@ssw0rd');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Emily Davis', 'emily.d@gmail.com', 40, '555-4321', 'StrongP@ss');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Michael Taylor', 'michael.t@gmail.com', 29, '555-8765', 'Taylor2023');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Olivia Brown', 'olivia.b@gmail.com', 33, '555-3456', 'OliviaPass');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Daniel White', 'daniel.w@gmail.com', 45, '555-6543', 'White@123');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Sophia Miller', 'sophia.m@gmail.com', 31, '555-7890', 'MillerPass');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Christopher Lee', 'chris.lee@gmail.com', 27, '555-2345', 'Lee2023');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Emma Turner', 'emma.turner@gmail.com', 38, '555-8765', 'E_TurnerPass');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Liam Carter', 'liam.c@gmail.com', 32, '555-9876', 'CarterPass');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Ava Hall', 'ava.hall@gmail.com', 41, '555-8765', 'Hall2023');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Logan Cooper', 'logan.coop@gmail.com', 26, '555-6543', 'CooperPass');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Mia Martin', 'mia.martin@gmail.com', 39, '555-2345', 'MiaPass123');
INSERT INTO public.customers (name, email, age, phone, password) VALUES ('Ethan Adams', 'ethan.adams@gmail.com', 30, '555-9876', 'AdamsPass');


INSERT INTO accounts (number,currency,balance,customer_id) VALUES ('jejk64cbeje22j','USD',100.0,1);
INSERT INTO accounts (number,currency,balance,customer_id) VALUES ('efjbe24avb3j','USD',1000.0,1);
INSERT INTO accounts (number,currency,balance,customer_id) VALUES ('evkl367nee','USD',0.0,2);
INSERT INTO accounts (number,currency,balance,customer_id) VALUES ('vekl3443senvn','EUR',100.0,2);
INSERT INTO accounts (number,currency,balance,customer_id) VALUES ('eklvne263slsv','EUR',20000.0,3);
INSERT INTO accounts (number,currency,balance,customer_id) VALUES ('sevek43snev','USD',135.0,3);

INSERT INTO companies (name,address) VALUES ('Google','California');
INSERT INTO companies (name,address) VALUES ('Microsoft','New-York');
INSERT INTO companies (name,address) VALUES ('Apple','Miami');

INSERT INTO customers_companies (customer_id, company_id) VALUES (1, 1),
                                                                        (1, 2),
                                                                        (2, 1),
                                                                        (3, 2),
                                                                        (3, 3);

INSERT INTO users(user_id, enabled, encrypted_password, user_name) VALUES
                                                                       (101, true, '$2a$10$BXH1wlAJPIMXvjnJTBoRuea4CvZwSs8/Zqz4bDRZBDJ6hxvXoHlqq', 'a'),
                                                                       (102, true, '$2y$10$cV7aw2ogADMipr5V7Q7rqu52AvaLOuHHx5y/t20k/ATPCWUELjQZW', 'admin');
INSERT INTO roles(role_id, role_name, user_id) VALUES
                                                   (101, 'USER', 101),
                                                   (102, 'ADMIN', 102);


COMMIT;