DROP TABLE IF EXISTS customers;
CREATE TABLE public.customers (
                                  id INT AUTO_INCREMENT  PRIMARY KEY,
                                  name VARCHAR(250) NOT NULL,
                                  password VARCHAR(250) NOT NULL,
                                  email VARCHAR(250) NOT NULL,
                                  phone VARCHAR(250) NOT NULL,
                                  age INT NOT NULL,
                                  creation_date TIMESTAMP  ,
                                  last_modified_date TIMESTAMP

);

DROP TABLE IF EXISTS accounts;
CREATE TABLE public.accounts (
                                  id INT AUTO_INCREMENT  PRIMARY KEY,
                                  number VARCHAR(250) NOT NULL,
                                  currency VARCHAR(3) NOT NULL,
                                  balance DOUBLE PRECISION  DEFAULT 0,
                                  customer_id INTEGER REFERENCES customers(id),
                                      creation_date TIMESTAMP  ,
                                  last_modified_date TIMESTAMP

);


DROP TABLE IF EXISTS companies;
CREATE TABLE public.companies (
                                  id INTEGER AUTO_INCREMENT PRIMARY KEY,
                                  name VARCHAR(250) NOT NULL,
                                  address VARCHAR(250) NOT NULL,
                                  creation_date TIMESTAMP  ,
                                  last_modified_date TIMESTAMP
);

DROP TABLE IF EXISTS customers_companies CASCADE;
CREATE TABLE public.customers_companies (
                                           id INTEGER AUTO_INCREMENT PRIMARY KEY,
                                           customer_id INTEGER REFERENCES customers (id) NOT NULL,
                                           company_id INTEGER REFERENCES companies (id) NOT NULL,
                                           creation_date TIMESTAMP  ,
                                           last_modified_date TIMESTAMP
);

DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users (
                       user_id INT AUTO_INCREMENT  PRIMARY KEY,
                       user_name VARCHAR(36) NOT NULL,
                       encrypted_password VARCHAR(128) NOT NULL,
                       creation_date TIMESTAMP  NULL,
                       last_modified_date TIMESTAMP  NULL,
                       created_by INT NULL,
                       last_modified_by INT NULL,
                       enabled boolean NOT NULL
);

DROP TABLE IF EXISTS roles CASCADE;
CREATE TABLE roles (
                       role_id INT AUTO_INCREMENT  PRIMARY KEY,
                       role_name VARCHAR(30) NOT NULL,
                       user_id INT
);



